module gitlab.com/Munnotubbel/pi-motion-detection

go 1.16

require (
	github.com/stianeikeland/go-rpio v3.0.0+incompatible // indirect
	gobot.io/x/gobot v1.15.0 // indirect
)
